package cz.cvut.fel.omo.trackingSystem;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 *
 * @author balikm1
 */
public class VehicleTest {
    @Test
    public void drive_distanceMoreThanZero_mileageIncremetedByTraveledDistance()
    {
        // arrange
        int initialMileage = 500;
        int traveledDistance = 275;
        int expectedValue = initialMileage + traveledDistance;
        Vehicle vehicle = new Vehicle("Fiat", "LKASJDLK15", initialMileage);

        // act
        vehicle.drive(traveledDistance);
        int actualMileage = vehicle.getMileage();

        // assert
        assertEquals(expectedValue, actualMileage);
    }
}