package cz.cvut.fel.omo.trackingSystem;

import org.apache.commons.collections4.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuki on 22/09/2017.
 * GPSTrackingSystem class represents the newly introduced tool for gaining control over company car park.
 */
public class GpsTrackingSystem {
    private static int counter = 0;
    private List<Tracker> activeTrackers = new ArrayList<>();

    public int calculateTotalMileage() {
        int totalMileage = 0;
        for (Tracker tracker : activeTrackers)
            totalMileage += tracker.getTrackerMileage();
        return totalMileage;
    }

    public void attachTrackingDevices(List<Vehicle> carPark) {
        if (CollectionUtils.isNotEmpty(carPark)) {
            for (Vehicle vehicle : carPark) {
                Tracker newTracker = new Tracker(counter++);
                newTracker.attachTracker(vehicle);
                activeTrackers.add(newTracker);
            }
        }
    }

    public static int getCounter(){
        return counter;
    }

    public void generateMonthlyReport(){
        System.out.println("========= GPS tracking system: Monthly report ========= ");
        System.out.println("Currently active devices:");
        activeTrackers.forEach(tracker -> {System.out.print(tracker); tracker.resetTrackerMileage();});
        System.out.println("\nThis month traveled distance:" + calculateTotalMileage());
    }
}
