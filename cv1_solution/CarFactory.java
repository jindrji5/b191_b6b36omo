
import java.util.Calendar;

public class CarFactory {
    private int carsMade = 0;

    public int getCarsMade() {
        return carsMade;
    }

    private void makingCar(String label){
        carsMade++;
        System.out.println(label);

    }
    Car vytvorNakladni(int color){
        makingCar("Vytvarim nakladni auto");
        return new Car(6,50, Calendar.getInstance().get(Calendar.YEAR),color);
    }
    Car vytvorOsobni(int color){
        makingCar("Vytvarim osobni auto");
        return new Car(4,17, Calendar.getInstance().get(Calendar.YEAR), color);
    }
}
